<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HomeController extends AbstractController
{
    private $http;

    public function __construct(HttpClientInterface $http)
    {
        $this->http = $http;
    }
    
    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        $response = $this->http->request('GET', 'http://localhost:8000/api/people');
        if ($response->getStatusCode() == 200)
            return $this->render('home/index.html.twig', [
                'people' => $response->toArray(),
            ]);
        else
            return new Response("error :(", Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
